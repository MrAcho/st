# Luke's build of st + Catppuccino Theme

## Dependencies
+    libxft-bgra
+    libXft
+    libX11
+    fontconfig
+    xcompmgr 
+    picom
+    xlib
+    (i really don't know which ones are absolutely needed but having them all doesn't hurt **a lot**)

## Applied st patches

+ Boxdraw
+ Ligatures
+ font2
+ updated to latest version 0.8.4

## Bindings for

+ **scrollback** with `alt-↑/↓` or `alt-pageup/down` or `shift` while scrolling the
  mouse.
+ OR **vim-bindings**: scroll up/down in history with `alt-k` and `alt-j`.
  Faster with `alt-u`/`alt-d`.
+ **zoom/change font size**: same bindings as above, but holding down shift as
  well. `alt-home` returns to default
+ **copy text** with `alt-c`, **paste** is `alt-v` or `shift-insert`

The `alpha` value (for transparency) goes from `0` (transparent) to `1`
(opaque). There is an example `Xdefaults` file in this respository.

